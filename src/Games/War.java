package Games;

import Cards.Card;
import Cards.CardModule;
import Cards.Deck;
import Cards.Hand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Brandon Faulkner on 8/17/16.
 */
public class War {
    private static int riskAmountOfCards = 3;
    private static int numTurns = 0;
    private static int p1IntValue;
    private static int p2IntValue;
    private static Deck player1Draw;
    private static Deck player2Draw;
    private static Deck player1Discard;
    private static Deck player2Discard;
    private static boolean restart = false;
    private static String winner = "none";
    private static boolean lastCard = false;
    private static Scanner scanMan = new Scanner(System.in);
    private static Hand p1hand, p2hand;

    public static void main(String[] args) throws InterruptedException, IOException {
        boolean stillPlaying = true;

        printOutTitle();

        while (stillPlaying) {
            lastCard = false;
            restart = false;
            numTurns = 0;
            Deck deck = new Deck();
            deck.shuffle();
            for (Card card : (Iterable<Card>) deck.getItsCards()) {
                card.isFaceUp = false;
            }
            player1Draw = deck.cut(26);
            player2Draw = deck.clone();
            player1Discard = new Deck("Empty");
            player2Discard = new Deck("Empty");

            System.out.println("Thank you for playing.\nEnter \"start\" to begin.\nEnter \"options\" at any time to change options.\nEnter \"quit\" to quit\n");
            String input = scanMan.nextLine();
            nextMove(input);
        }
    }

    private static void beginWar(Card p1Card, Card p2Card) throws InterruptedException, IOException {
        clearDisplay();
        printOutTurn();
        System.out.println("Tie! Prepare for war!\n\n");
        printOutHands();
        checkDeck(player1Draw, player1Discard, 1);
        p1hand.addCard(player1Draw.draw());
        checkDeck(player2Draw, player2Discard, 2);
        p2hand.addCard(player2Draw.draw());
        Thread.sleep(1000);
        clearDisplay();
        for (int i = 0; i < riskAmountOfCards; i++) {
            checkDeck(player1Draw, player1Discard, 1);
            checkDeck(player2Draw, player2Discard, 2);
            printOutTurn();
            System.out.println("War is Raging!\n\n");
            printOutHands();
            p1hand.addCard(player1Draw.draw());
            p2hand.addCard(player2Draw.draw());
            Thread.sleep(500);
            clearDisplay();
        }
        p1Card = p1hand.getItsCards().lastElement();
        p1Card.isFaceUp = true;
        p2Card = p2hand.getItsCards().lastElement();
        p2Card.isFaceUp = true;
        printOutTurn();
        printOutStats();
        printOutHands();

        assignIntValue(p1Card, 1);
        assignIntValue(p2Card, 2);
        Thread.sleep(1000);
        compareCards(p1Card, p2Card);
        return;
    }

    private static void compareCards(Card p1Card, Card p2Card) throws IOException, InterruptedException {
        if (p1IntValue == p2IntValue) {//Tie
            winner = "none";
            beginWar(p1Card, p2Card);
        } else if (p1IntValue < p2IntValue) {//P2 Wins round
            winner = "Player 2";
            putCardsInDiscardPile(p1Card, p2Card, player2Discard);
            printOutWinner();
        } else {//P1 Wins round
            winner = "Player 1";
            putCardsInDiscardPile(p1Card, p2Card, player1Discard);
            printOutWinner();
        }
        return;
    }

    private static void openOptions() throws IOException, InterruptedException {
        Scanner optionsScan = new Scanner(System.in);
        System.out.println("   ┌─────────────────────────Options─────────────────────────┐");
        System.out.println("   │ risk <1 or 3>      --     Number of cards to risk in tie│");
        System.out.println("   │ exit               --               Return to start menu│");
        System.out.println("   │ restart            --                   Restart the game│");
        System.out.println("   │ quit               --                      Quit the game│");
        System.out.println("   └─────────────────────────────────────────────────────────┘");
        String next = optionsScan.next();
        optionsScan.reset();
        switch (next.toLowerCase()){
            case "risk":
            case "r":
                String choice = optionsScan.next();
                optionsScan.reset();
                riskAmountOfCards = choice.equals("1")? 1 : 3;
                break;
            case "exit":
            case "e":
                nextMove("same");
                break;
            case "res":
            case "restart":
                restart = true;
                break;
            case "quit":
            case "q":
                System.out.println("Goodbye.");
                System.exit(1);
            default:
                clearDisplay();
                openOptions();
                break;
        }
    }

    private static void assignIntValue(Card card, int player){
        int value = 0;
        switch(card.getItsValue()){
            case "A":
                value += 1;
            case "K":
                value += 1;
            case "Q":
                value += 1;
            case "J":
                value += 11;
                break;
            default:
                value = Integer.parseInt(card.getItsValue());
                break;
        }
        if (player == 1){
            p1IntValue = value;
        }else {
            p2IntValue = value;
        }
    }

    private static void clearDisplay() throws IOException {
        final String os = System.getProperty("os.name");
        Process proc;
        if (os.contains("Windows")) {
            proc = Runtime.getRuntime().exec("cls");
        } else {
            proc = Runtime.getRuntime().exec("clear");
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()))) {

            String line = "";
            while ((line = reader.readLine()) != null) {
                System.out.print(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            proc.waitFor();
            printOutTitle();
            printOutOptions();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    private static void printOutTitle(){
        System.out.println(
                "\n     █▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█\n" +
                  "                         █   █ █▀▀█ █▀▀█ \n" +
                  "                         █▄█▄█ █▄▄█ █▄▄▀ \n" +
                  "                          ▀ ▀  ▀  ▀ ▀ ▀▀ \n" +
                  "     █▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄█\n");
    }

    private static void printOutOptions(){
        System.out.println("   \"Enter\" - Continue / \"o\" - Options / \"q\" - Quit / r - Restart\n");
    }

    private static void printOutStats(){
        System.out.println("Player 1 Card total: " + (player1Discard.getItsCards().size() + player1Draw.getItsCards().size()));
        System.out.println("Player 2 Card total: " + (player2Discard.getItsCards().size() + player2Draw.getItsCards().size()) + "\n");
    }

    private static void nextMove(String input) throws InterruptedException, IOException {
        if (!restart) {
            switch (input.toLowerCase()) {
                case "same":
                    if (numTurns > 0) {
                        clearDisplay();
                        printOutTurn();
                        printOutStats();
                        Card p1Card = p1hand.getItsCards().lastElement();
                        p1Card.isFaceUp = true;
                        Card p2Card = p2hand.getItsCards().lastElement();
                        p2Card.isFaceUp = true;
                        printOutHands();
                        printOutWinner();
                        String next = scanMan.nextLine();
                        nextMove(next);
                    }else{
                        return;
                    }
                    break;
                case "r":
                case "restart":
                    restart = true;
                    break;
                case "options":
                case "o":
                    clearDisplay();
                    openOptions();
                    break;
                case "quit":
                case "q":
                    System.out.println("Goodbye.");
                    System.exit(1);
                default:
                    clearDisplay();
                    playNextTurn();
                    break;
            }
        }
    }

    private static void clearHands(){
        for (int i = 0; i < p1hand.getItsCards().size(); i++){
            p1hand.getItsCards().remove(i);
            p2hand.getItsCards().remove(i);
        }
    }

    private static void putCardsInDiscardPile(Card p1Card, Card p2Card, Deck discard){
        if (p1hand.getItsCards().size() == 0){
            p1Card.isFaceUp = false;
            p2Card.isFaceUp = false;
            discard.add(p1Card);
            discard.add(p2Card);
        }else {
            for (Card card : (Iterable<Card>) p1hand.getItsCards()) {
                card.isFaceUp = false;
                discard.add(card);
            }
            for (Card card : (Iterable<Card>) p2hand.getItsCards()) {
                card.isFaceUp = false;
                discard.add(card);
            }
        }
    }

    private static void playNextTurn() throws InterruptedException, IOException {
        p1hand = new Hand();
        p2hand = new Hand();
        numTurns++;

//        if (p1hand.getItsCards().size() != 0){clearHands();}
        printOutTurn();
        printOutStats();
        checkDeck(player1Draw, player1Discard, 1);
        Card p1Card = player1Draw.draw();
        checkDeck(player2Draw, player2Discard, 2);
        Card p2Card = player2Draw.draw();

        p1Card.isFaceUp = true;
        p2Card.isFaceUp = true;
        if (p1hand.getItsCards().size() == 0) {p1hand.addCard(p1Card);}
        if (p2hand.getItsCards().size() == 0) {p2hand.addCard(p2Card);}
        printOutHands();
        assignIntValue(p1Card, 1);
        assignIntValue(p2Card, 2);
        Thread.sleep(1000);
        compareCards(p1Card, p2Card);
        String next = scanMan.nextLine();
        nextMove(next);
    }

    private static void checkDeck(Deck deck, Deck discard, int player) throws IOException, InterruptedException {
        int deckSize = deck.getItsCards().size();
        int discardSize = discard.getItsCards().size();

        if (deckSize == 1 && discardSize == 0){
            lastCard = true;
        }
        if (deckSize == 0){
            if (discardSize == 0){
                clearDisplay();
                System.out.println("Player " + ((player == 1)? "1" : "2") + " is out of cards." + ((player == 1)? "2" : "1") + " wins!!");
                gameOver();
            }else if (discardSize == 1){
                lastCard = true;
            }
            int iter = discardSize;
            for (int i = 0; i < iter; i++){
                deck.add(discard.draw());
            }
            deck.shuffle();
        }
    }

    private static void gameOver() throws IOException, InterruptedException {
        System.out.println("Game over.\n\n");
        return;
    }

    private static void printOutHands(){
        if (p1hand.getItsCards().size() > 0) {
            System.out.println("──Player 1──");
            System.out.println(CardModule.toSymbol(p1hand.getItsCards()));
            System.out.println("──Player 2──");
            System.out.println(CardModule.toSymbol(p2hand.getItsCards()));
        }
    }

    private static void printOutTurn(){
        System.out.println("      ────────────────────────Turn " + numTurns + "────────────────────────\n");
    }

    private static void printOutWinner(){
        if (!winner.equals("none")) {
            System.out.println("\n" + winner + " wins round " + numTurns + "!\n");
        }
    }
}

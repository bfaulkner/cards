package Games;

import Cards.Card;
import Cards.CardModule;
import Cards.Deck;
import Cards.Hand;

import java.util.Scanner;
import java.util.Stack;

public class BlackJack {
    public static void main(String[] args) {
        boolean stillPlaying = true;
        Scanner scanMan = new Scanner(System.in);
        int numGames = 0;
        int thinkTime = 1000;
        int playersTotal = 0;
        int dealersTotal = 0;

        p("------------------------------------------------------------------------------------");
        p("---|||||---||-------||||----|||||--||--||--------------||---||||----|||||--||--||---");
        p("---||--||--||------||--||--||------||-||---------------||--||--||--||------||-||----");
        p("---|||||---||------||||||--||------||||----------------||--||||||--||------||||-----");
        p("---||--||--||------||--||--||------||-||-----------||--||--||--||--||------||-||----");
        p("---|||||---||||||--||--||---|||||--||--||-----------||||---||--||---|||||--||--||---");
        p("------------------------------------------------------------------------------------");

        while (stillPlaying) {
            try {
                if (++numGames < 10) {
                    p("\n=================================");
                    p("============ GAME 0" + numGames + " ============");
                } else {
                    p("=================================");
                    p("============ GAME " + numGames + " ============");
                }
                if (playersTotal < 10 && dealersTotal < 10) {
                    p("========= D: 0" + dealersTotal + " | P: 0" + playersTotal + " =========");
                } else if (playersTotal < 10) {
                    p("========= D: " + dealersTotal + " | P: 0" + playersTotal + " =========");
                } else if (dealersTotal < 10) {
                    p("========= D: 0" + dealersTotal + " | P: " + playersTotal + " =========");
                } else {
                    p("========= D: " + dealersTotal + " | P: " + playersTotal + " =========");
                }
                p("=================================");
                Thread.sleep(thinkTime);

                Deck deck = new Deck();
                deck.shuffle();
                Hand[] players = deck.deal(2, 2);
                players[0].setItsName("DEALER");
                players[1].setItsName("PLAYER");
                players[0].getItsCards().get(0).isFaceUp = false;
                for (Hand player : players) {
                    CardModule.organize(player.getItsCards(), "fvs");
                }

                int dealersScore = calculateTotal(players[0]);
                int playersScore = calculateTotal(players[1]);

                p("DEALER'S HAND:");
                p(CardModule.toSymbol(players[0].getItsCards()));
                p(players[0].toString());
                Thread.sleep(thinkTime);

                p("\n--------- PLAYER'S TURN ---------");
                boolean myTurn = true;
                p(CardModule.toSymbol(players[1].getItsCards()));
                p(players[1].toString() + " {" + playersScore + "}");
                Thread.sleep(thinkTime / 2);
                while (myTurn) {
                    p("Hit or Stand?");
                    String move = scanMan.nextLine();
                    if (move.toLowerCase().equals("hit") || move.toLowerCase().equals("h") || move.equals("")) {
                        players[1].addCard(deck.draw());
                        CardModule.organize(players[1].getItsCards(), "fvs");
                        playersScore = calculateTotal(players[1]);
                        p(CardModule.toSymbol(players[1].getItsCards()));
                        p(players[1].toString() + " {" + playersScore + "}");
                        Thread.sleep((thinkTime / 2));
                    } else if (move.toLowerCase().equals("stand") || move.toLowerCase().equals("s")) {
                        myTurn = false;
                    } else {
                        p("Invalid response, try again.");
                    }
                }

                p("\n--------- DEALER'S TURN ---------");
                p(CardModule.toSymbol(players[0].getItsCards()));
                p(players[0].toString());
                Thread.sleep(thinkTime);
                if ((dealersScore > 16 && !hasAces(players[0])) || (dealersScore > 17 && hasAces(players[0]))) {
                    p("stand");
                    Thread.sleep(thinkTime);
                } else {
                    while ((dealersScore < 17 && !hasAces(players[0])) || (dealersScore < 18 && hasAces(players[0]))) {
                        p("hit");
                        players[0].addCard(deck.draw());
                        CardModule.organize(players[0].getItsCards(), "fvs");
                        dealersScore = calculateTotal(players[0]);
                        p(CardModule.toSymbol(players[0].getItsCards()));
                        p(players[0].toString());
                        Thread.sleep(thinkTime);
                    }
                    p("stand");
                    Thread.sleep(thinkTime);
                }

                p("\n------------ RESULTS ------------");
                p("PLAYERS HAND:");
                p(CardModule.toSymbol(players[1].getItsCards()));
                p(players[1].toString() + " {" + playersScore + "}");
                if (playersScore == 21 && players[1].getItsCards().size() == 2) {
                    p("Player black jack!");
                } else if (playersScore > 21) {
                    p("Player busts!");
                    p("Dealers hand remains unplayed.");
                    p("Player Loses!\n");
                    dealersTotal++;
                }

                if (playersScore < 22) {
                    p("DEALER'S HAND:");
                    for (Card c : players[0].getItsCards()) {
                        if (!c.isFaceUp) {
                            c.isFaceUp = true;
                        }
                    }
                    p(CardModule.toSymbol(players[0].getItsCards()));
                    p(players[0].toString() + " {" + dealersScore + "}");
                    if (dealersScore == 21 && players[0].getItsCards().size() == 2) {
                        p("Dealer black jack!");
                    }
                    if (dealersScore > 21) {
                        p("Dealer busts!");
                        p("Player Wins!\n");
                        playersTotal++;
                    } else if (playersScore > dealersScore) {
                        p("Player Wins!\n");
                        playersTotal++;
                    } else if (playersScore < dealersScore) {
                        p("Player Loses!\n");
                        dealersTotal++;
                    } else {
                        p("Push!\n");
                    }
                }
                Thread.sleep(thinkTime);

                p("Play again? (y/n)");
                String res = scanMan.nextLine();
                switch(res.toLowerCase()) {
                    case "yes":
                    case "y":
                    case "":
                        break;
                    default:
                        p("---------- FINAL SCORE ----------");
                        p("DEALER: " + dealersTotal);
                        p("PLAYER: " + playersTotal);
                        p("\nThanks for playing!");
                        stillPlaying = false;
                        break;
                }
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    private static boolean hasAces(Hand par) {
        boolean ret = false;

        for (Card c : par.getItsCards()) {
            if (c.getItsValue().equals("A")) {
                ret = true;
            }
        }

        return ret;
    }

    private static int calculateTotal(Hand par) {
        int ret = 0;
        int aces = 0;
        Stack<Card> parCards = par.getItsCards();

        for (Card c : parCards) {
            switch (c.getItsValue()) {
                case "10":
                case "J":
                case "Q":
                case "K":
                    ret += 10;
                    break;
                case "A":
                    aces++;
                    break;
                default:
                    ret += Integer.parseInt(c.getItsValue());
                    break;
            }
        }

        if (aces != 0) {
            int out[] = new int[aces + 1];
            if (aces == 1) {
                out[0] = 1;
                out[1] = 11;
            } else if (aces == 2) {
                out[0] = 1 + 1;
                out[1] = 11 + 1;
                out[2] = 11 + 11;
            } else if (aces == 3) {
                out[0] = 1 + 1 + 1;
                out[1] = 11 + 1 + 1;
                out[2] = 11 + 11 + 1;
                out[3] = 11 + 11 + 11;
            } else {
                out[0] = 1 + 1 + 1 + 1;
                out[1] = 11 + 1 + 1 + 1;
                out[2] = 11 + 11 + 1 + 1;
                out[3] = 11 + 11 + 11 + 1;
                out[4] = 11 + 11 + 11 + 11;
            }

            int mostEfficient = out[0];
            for (int i = 1; i < aces + 1; i++) {
                if ((ret + out[i]) < 22) {
                    mostEfficient = out[i];
                }
            }

            ret += mostEfficient;
        }

        return ret;
    }

    private static void p(String par) {
        p(par, true);
    }

    private static void p(String par, boolean ln) {
        if (ln) {
            System.out.println(par);
        } else {
            System.out.print(par);
        }
    }
}
package Cards;

public class Card {
    private static int numCards = 0;
    private final String itsName;
    private final String itsSuit;
    private final String itsValue;
    public boolean isFaceUp;

    public Card() {
        this("J", "K", true);
    }
    
    public Card(String suit, String value, boolean faceUp) {
        itsName = suit + value;
        itsSuit = suit;
        itsValue = value;
        isFaceUp = faceUp;
        numCards++;
    }
    
    public static int getNumCards() {
        return numCards;
    }

    public String getItsName() {
        return itsName;
    }

    public String getItsSuit() {
        return itsSuit;
    }

    public String getItsValue() {
        return itsValue;
    }

    public String toString() {
        return !isFaceUp ? "[▒▒]" : "[" + itsName + "]";
    }

    public void flip() {
        this.isFaceUp = !this.isFaceUp;
    }
}

package Cards;

import java.util.Stack;

public class CardSymbol extends Card {
    private String suit;
    private String value;

    public CardSymbol() {
        this("J", "K");
    }

    public CardSymbol(String suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    public String toString() {
        if (value.equals("") && suit.equals("")) {
            return
                    "┌─────────┐\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "└─────────┘\n";
        } else if (suit.equals("J")) {
            return
                    "┌─────────┐\n" +
                            "│J        │\n" +
                            "│O        │\n" +
                            "│K   *   J│\n" +
                            "│E  *J*  O│\n" +
                            "│R   *   K│\n" +
                            "│        E│\n" +
                            "│        R│\n" +
                            "└─────────┘\n";
        } else if (value.equals("10")) {
            return
                    "┌─────────┐\n" +
                            "│" + value + suit + "      │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + suit + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│      " + suit + value + "│\n" +
                            "└─────────┘\n";
        } else {
            return
                    "┌─────────┐\n" +
                            "│" + value + suit + "       │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + suit + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│       " + suit + value + "│\n" +
                            "└─────────┘\n";
        }
    }

    public static String toString(Stack<Card> hand) {
        String handStr = "";

        for (int i = 0; i < hand.size(); i++) {
            handStr += "┌───";
        }
        handStr += "──────┐\n";

        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("10")) {
                handStr += "│" + card.getItsValue() + card.getItsSuit();
            } else if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│J  ";
            } else {
                handStr += "│" + card.getItsValue() + card.getItsSuit() + " ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│O  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│K  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand.get(hand.size() - 1).getItsSuit().equals("J")) {
            handStr += " *   J│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│E  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand.get(hand.size() - 1).getItsSuit().equals("J")) {
            handStr += "*J*  O│\n";
        } else {
            handStr += " " + hand.get(hand.size() - 1).getItsSuit() + "    │\n";
        }
        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│R  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand.get(hand.size() - 1).getItsSuit().equals("J")) {
            handStr += " *   K│\n";
        } else {
            handStr += "      │\n";
        }

        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand.get(hand.size() - 1).getItsSuit().equals("J")) {
            handStr += "     E│\n";
        } else {
            handStr += "      │\n";
        }

        for (Card card : hand) {/////////////////////////////////////////////FOR
            if (card.getItsValue().equals("")) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (hand.get(hand.size() - 1).getItsValue().equals("10")) {
            handStr += "   " + hand.get(hand.size() - 1).getItsSuit() + hand.get(hand.size() - 1).getItsValue() + "│\n";
        } else if (hand.get(hand.size() - 1).getItsValue().equals("")) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand.get(hand.size() - 1).getItsSuit().equals("J")) {
            handStr += "     R│\n";
        } else {
            handStr += "    " + hand.get(hand.size() - 1).getItsSuit() + hand.get(hand.size() - 1).getItsValue() + "│\n";
        }
        for (Card card : hand) {/////////////////////////////////////////////FOR
            handStr += "└───";
        }
        handStr += "──────┘\n";

        return handStr;
    }

    public String getItsSuit() {
        return this.suit;
    }

    public String getItsValue() {
        return this.value;
    }
}

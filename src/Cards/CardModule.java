package Cards;

import java.util.Stack;

public class CardModule {
    public static String toSymbol(Card par) {
        if (!par.isFaceUp) {
            return
                    "┌─────────┐\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "└─────────┘";
        } else if (par.getItsSuit().equals("J")) {
            return
                    "┌─────────┐\n" +
                            "│J        │\n" +
                            "│O        │\n" +
                            "│K   *   J│\n" +
                            "│E  *J*  O│\n" +
                            "│R   *   K│\n" +
                            "│        E│\n" +
                            "│        R│\n" +
                            "└─────────┘";
        } else if (par.getItsValue().equals("10")) {
            return
                    "┌─────────┐\n" +
                            "│" + par.getItsValue() + par.getItsSuit() + "      │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + par.getItsSuit() + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│      " + par.getItsSuit() + par.getItsValue() + "│\n" +
                            "└─────────┘";
        } else {
            return
                    "┌─────────┐\n" +
                            "│" + par.getItsValue() + par.getItsSuit() + "       │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + par.getItsSuit() + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│       " + par.getItsSuit() + par.getItsValue() + "│\n" +
                            "└─────────┘";
        }
    }
    
    public static String toSymbol(Stack<Card> par) {
        String handStr = "";

        for (int i = 0; i < par.size(); i++) {
            handStr += "┌───";
        }
        handStr += "──────┐\n";

        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else if (card.getItsValue().equals("10")) {
                handStr += "│" + card.getItsValue() + card.getItsSuit();
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│J  ";
            } else {
                handStr += "│" + card.getItsValue() + card.getItsSuit() + " ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│O  ";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│K  ";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (par.get(par.size() - 1).getItsSuit().equals("J")) {
            handStr += " *   J│\n";
        } else {
            handStr += "      │\n";
        }
        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│E  ";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (par.get(par.size() - 1).getItsSuit().equals("J")) {
            handStr += "*J*  O│\n";
        } else {
            handStr += " " + par.get(par.size() - 1).getItsSuit() + "    │\n";
        }
        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else if (card.getItsSuit().equals("J")) {
                handStr += "│R  ";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (par.get(par.size() - 1).getItsSuit().equals("J")) {
            handStr += " *   K│\n";
        } else {
            handStr += "      │\n";
        }

        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (par.get(par.size() - 1).getItsSuit().equals("J")) {
            handStr += "     E│\n";
        } else {
            handStr += "      │\n";
        }

        for (Card card : par) {
            if (!card.isFaceUp) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (!par.get(par.size() - 1).isFaceUp) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (par.get(par.size() - 1).getItsValue().equals("10")) {
            handStr += "   " + par.get(par.size() - 1).getItsSuit() + par.get(par.size() - 1).getItsValue() + "│\n";
        } else if (par.get(par.size() - 1).getItsSuit().equals("J")) {
            handStr += "     R│\n";
        } else {
            handStr += "    " + par.get(par.size() - 1).getItsSuit() + par.get(par.size() - 1).getItsValue() + "│\n";
        }
        for (Card ignored : par) {
            handStr += "└───";
        }
        handStr += "──────┘";

        return handStr;
    }

    private static int getNumIters(int[] iter) {
        int ret = 0;

        for (int i : iter) {
            if (i != 0) {
                ret++;
            }
        }

        return ret;
    }

    private static int[] caseF(Stack<Card> par, int[] iter) {
        int curIter = 0;
        boolean curFlip = par.get(0).isFaceUp;
        int n = 0;
        for (Card c : par) {
            if (c.isFaceUp == curFlip) {
                iter[curIter] = n;
            } else {
                curFlip = c.isFaceUp;
                curIter++;
            }
            n++;
        }
        return iter;
    }

    private static int[] caseV(Stack<Card> par, int[] iter) {
        int curIter = 0;
        String curValue = par.get(0).getItsValue();
        int n = 0;
        for (Card c : par) {
            if (c.getItsValue().equals(curValue)) {
                iter[curIter] = n;
            } else {
                curValue = c.getItsValue();
                curIter++;
            }
            n++;
        }
        return iter;
    }

    private static int[] caseS(Stack<Card> par, int[] iter) {
        int curIter = 0;
        String curSuit = par.get(0).getItsSuit();
        int n = 0;
        for (Card c : par) {
            if (c.getItsSuit().equals(curSuit)) {
                iter[curIter] = n;
            } else {
                curSuit = c.getItsSuit();
                curIter++;
            }
            n++;
        }
        return iter;
    }

    public static boolean organize(Stack<Card> par) {
        return organize(par, "vfs");
    }

    public static boolean organize(Stack<Card> par, String mode) {
        return organize(par, mode, false);
    }

    public static boolean organize(Stack<Card> par, String mode, boolean verbose) {
        boolean ret = true;
        for (int i = 0; i < mode.length(); i++) {
            if (ret) {
                if (mode.charAt(i) == 'f') {
                    ret = orgF(par, mode.substring(0, i));
                } else if (mode.charAt(i) == 's') {
                    ret = orgS(par, mode.substring(0, i));
                } else if (mode.charAt(i) == 'v') {
                    ret = orgV(par, mode.substring(0, i));
                } else {
                    ret = false;
                }
            } else {
                return false;
            }
        }

        if (verbose) {
            System.out.println("ORGANIZE COMPLETE.");
        }

        return true;
    }

    private static boolean orgF(Stack<Card> par, String prev) {
        Hand tempHand = new Hand("tempHand");
        boolean changeWasMade = true;

        int[] iter = new int[par.size()];

        int curIter = 0;
        int numIters;
        switch (prev) {
            case "":
                iter[curIter] = par.size() - 1;
                break;
            case "s": {
                iter = caseS(par, iter);
                break;
            }
            case "v": {
                iter = caseV(par, iter);
                break;
            }
            case "sv":
            case "vs":
                String curSuit = par.get(0).getItsSuit();
                String curValue = par.get(0).getItsValue();
                int n = 0;
                for (Card c : par) {
                    if (c.getItsSuit().equals(curSuit) && c.getItsValue().equals(curValue)) {
                        iter[curIter] = n;
                    } else {
                        curSuit = c.getItsSuit();
                        curValue = c.getItsValue();
                        curIter++;
                        iter[curIter] = n;
                    }
                    n++;
                }
                break;
            default:
                return false;
        }
        numIters = getNumIters(iter);


        while (changeWasMade) {
            changeWasMade = false;
            for (int j = 0; j < numIters; j++) {
                for (int i = j == 0 ? 0 : iter[j - 1] + 1; i < iter[j]; i++) {

                    boolean c1 = par.get(i).isFaceUp;
                    boolean c2 = par.get(i + 1).isFaceUp;

                    if (!c1) {
                        if (c2) {
                            changeWasMade = true;
                            tempHand.addCard(par.get(i));
                            par.set(i, par.get(i + 1));
                            par.set(i + 1, tempHand.getItsCards().get(0));
                            tempHand.getItsCards().remove(0); //?
                        }
                    }
                }
            }
        }

        return true;
    }

    private static boolean orgS(Stack<Card> par, String prev) {
        Hand tempHand = new Hand("tempHand");
        boolean changeWasMade = true;

        int[] iter = new int[par.size()];

        int curIter = 0;
        int numIters;
        switch (prev) {
            case "":
                iter[curIter] = par.size() - 1;
                break;
            case "f": {
                iter = caseF(par, iter);
                break;
            }
            case "v": {
                iter = caseV(par, iter);
                break;
            }
            case "fv":
            case "vf":
                boolean curFlip = par.get(0).isFaceUp;
                String curValue = par.get(0).getItsValue();
                int n = 0;
                for (Card c : par) {
                    if (c.isFaceUp == curFlip && c.getItsValue().equals(curValue)) {
                        iter[curIter] = n;
                    } else {
                        curFlip = c.isFaceUp;
                        curValue = c.getItsValue();
                        curIter++;
                        iter[curIter] = n;
                    }
                    n++;
                }
                break;
            default:
                return false;
        }
        numIters = getNumIters(iter);

        while (changeWasMade) {
            changeWasMade = false;
            for (int j = 0; j < numIters; j++) {
                for (int i = j == 0 ? 0 : iter[j - 1] + 1; i < iter[j]; i++) {
                    String c1 = par.get(i).getItsSuit();
                    String c2 = par.get(i + 1).getItsSuit();

                    switch (c1) {
                        //♠♥♣♦
                        case "♠":
                            break;
                        case "♥":
                            switch (c2) {
                                case "♠":
                                    tempHand.addCard(par.get(i));
                                    par.set(i, par.get(i + 1));
                                    par.set(i + 1, tempHand.getItsCards().get(0));
                                    tempHand.getItsCards().remove(0);
                                    changeWasMade = true;
                                    break;
                                case "♥":
                                case "♣":
                                case "♦":
                                default:
                                    break;
                            }
                            break;
                        case "♣":
                            switch (c2) {
                                case "♠":
                                case "♥":
                                    tempHand.addCard(par.get(i));
                                    par.set(i, par.get(i + 1));
                                    par.set(i + 1, tempHand.getItsCards().get(0));
                                    tempHand.getItsCards().remove(0);
                                    changeWasMade = true;
                                    break;
                                case "♣":
                                case "♦":
                                default:
                                    break;
                            }
                            break;
                        case "♦":
                            switch (c2) {
                                case "♠":
                                case "♥":
                                case "♣":
                                    tempHand.addCard(par.get(i));
                                    par.set(i, par.get(i + 1));
                                    par.set(i + 1, tempHand.getItsCards().get(0));
                                    tempHand.getItsCards().remove(0);
                                    changeWasMade = true;
                                    break;
                                case "♦":
                                default:
                                    break;
                            }
                            break;
                        default:
                            switch (c2) {
                                case "♠":
                                case "♥":
                                case "♣":
                                case "♦":
                                    tempHand.addCard(par.get(i));
                                    par.set(i, par.get(i + 1));
                                    par.set(i + 1, tempHand.getItsCards().get(0));
                                    tempHand.getItsCards().remove(0);
                                    changeWasMade = true;
                                    break;
                                default:
                                    break;
                            }
                            break;
                    }
                }
            }
        }

        return true;
    }

    private static  boolean orgV(Stack<Card> par, String prev) {
        Hand tempHand = new Hand("tempHand");
        boolean changeWasMade = true;

        int[] iter = new int[par.size()];

        int curIter = 0;
        int numIters;
        switch (prev) {
            case "":
                iter[curIter] = par.size() - 1;
                break;
            case "f": {
                iter = caseF(par, iter);
                break;
            }
            case "s": {
                iter = caseS(par, iter);
                break;
            }
            case "fs":
            case "sf":
                boolean curFlip = par.get(0).isFaceUp;
                String curSuit = par.get(0).getItsSuit();
                int n = 0;
                for (Card c : par) {
                    if (c.isFaceUp == curFlip && c.getItsSuit().equals(curSuit)) {
                        iter[curIter] = n;
                    } else {
                        curFlip = c.isFaceUp;
                        curSuit = c.getItsSuit();
                        curIter++;
                        iter[curIter] = n;
                    }
                    n++;
                }
                break;
            default:
                return false;
        }
        numIters = getNumIters(iter);


        while (changeWasMade) {
            changeWasMade = false;
            for (int j = 0; j < numIters; j++) {
                for (int i = j == 0 ? 0 : iter[j - 1] + 1; i < iter[j]; i++) {
                    String c1 = par.get(i).getItsValue();
                    String c2 = par.get(i + 1).getItsValue();

                    try {
                        if (Integer.parseInt(c1) < Integer.parseInt(c2)) {
                            tempHand.addCard(par.get(i));
                            par.set(i, par.get(i + 1));
                            par.set(i + 1, tempHand.getItsCards().get(0));
                            tempHand.getItsCards().remove(0);
                            changeWasMade = true;
                        }
                    } catch (Exception e) {
                        switch (c1) {
                            case "A":
                                break;
                            case "K":
                                switch (c2) {
                                    case "A":
                                        tempHand.addCard(par.get(i));
                                        par.set(i, par.get(i + 1));
                                        par.set(i + 1, tempHand.getItsCards().get(0));
                                        tempHand.getItsCards().remove(0);
                                        changeWasMade = true;
                                        break;
                                    case "K":
                                    case "Q":
                                    case "J":
                                    default:
                                        break;
                                }
                                break;
                            case "Q":
                                switch (c2) {
                                    case "A":
                                    case "K":
                                        tempHand.addCard(par.get(i));
                                        par.set(i, par.get(i + 1));
                                        par.set(i + 1, tempHand.getItsCards().get(0));
                                        tempHand.getItsCards().remove(0);
                                        changeWasMade = true;
                                        break;
                                    case "Q":
                                    case "J":
                                    default:
                                        break;
                                }
                                break;
                            case "J":
                                switch (c2) {
                                    case "A":
                                    case "K":
                                    case "Q":
                                        tempHand.addCard(par.get(i));
                                        par.set(i, par.get(i + 1));
                                        par.set(i + 1, tempHand.getItsCards().get(0));
                                        tempHand.getItsCards().remove(0);
                                        changeWasMade = true;
                                        break;
                                    case "J":
                                    default:
                                        break;
                                }
                                break;
                            default:
                                switch (c2) {
                                    case "A":
                                    case "K":
                                    case "Q":
                                    case "J":
                                        tempHand.addCard(par.get(i));
                                        par.set(i, par.get(i + 1));
                                        par.set(i + 1, tempHand.getItsCards().get(0));
                                        tempHand.getItsCards().remove(0);
                                        changeWasMade = true;
                                        break;
                                    default:
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
        }

        return true;
    }
}

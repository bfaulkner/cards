package Cards;

import java.util.Stack;

public class Hand {
    private static int numHands = 0;
    private String itsName;
    private Stack<Card> itsCards;

    public Hand() {
        this("HAND" + (numHands + 1));
    }

    public Hand(String name) {
        itsName = name;
        itsCards = new Stack<>();
        numHands++;
    }

    public static int getNumHands() {
        return numHands;
    }

    public String getItsName() {
        return itsName;
    }

    public Stack<Card> getItsCards() {
        return itsCards;
    }

    public void setItsName(String name) {
        itsName = name;
    }

    public String toString() {
        String ret = "" + itsName + ": [";

        for (int i = 0; i < itsCards.size(); i++) {
            if (i == 0) {
                ret += itsCards.get(i).toString();
            } else {
                ret += " " + itsCards.get(i).toString();
            }
        }

        ret += "]";

        return ret;
    }

    public void shuffle() {
        shuffle(100);
    }

    public void shuffle(int passes) {
        shuffle(passes, false);
    }

    public void shuffle(int passes, boolean verbose) {
        for (int k = 0; k < passes; k++) {
            for (int i = 0; i < itsCards.size(); i++) {
                int card2 = (int) (itsCards.size() * Math.random());
                while (card2 == i) {
                    card2 = (int) (itsCards.size() * Math.random());
                }
                Card temp = itsCards.get(i);
                itsCards.set(i, itsCards.get(card2));
                itsCards.set(card2, temp);
            }
        }
        if (verbose) {
            System.out.println(itsName + " SHUFFLED " + passes + " TIMES.");
        }
    }

    public void addCard(Card par) {
        itsCards.push(par);
    }

    public Card playCard(String cardName) {
        cardName = cardName.toLowerCase();
        cardName.replace('d', '♦');
        cardName.replace('c', '♣');
        cardName.replace('s', '♠');
        cardName.replace('h', '♥');
        for (Card card : this.getItsCards()) {
            if (card.getItsName().toLowerCase().equals(cardName)) {
                this.getItsCards().remove(this.getItsCards().indexOf(card));
                return card;
            }
        }
        return null;
    }

    public void flip() {
        for (Card c : itsCards) {
            c.flip();
        }
    }
}

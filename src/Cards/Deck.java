package Cards;

import java.util.Stack;

public class Deck {
    private static int numDecks = 0;
    private String itsName;
    private String[] itsSuits;
    private String[] itsValues;
    private int itsTotalNumCards;
    private Stack<Card> itsCards;

    public Deck() {
        this("Standard");
    }

    public Deck(String type) {
        this(type, "DECK" + (numDecks + 1));
    }

    public Deck(String type, String name) {
        itsName = name;
        itsCards = new Stack<>();

        if (type.toLowerCase().equals("standard")) {
            itsSuits = new String[]{"♠", "♣", "♥", "♦"};
            itsValues = new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
            itsTotalNumCards = itsSuits.length * itsValues.length;

            for (String curSuit : itsSuits) {
                for (String curValue : itsValues) {
                    itsCards.push(new Card(curSuit, curValue, true));
                }
            }
        } else if (type.toLowerCase().equals("empty")) {
            itsSuits = null;
            itsValues = null;
            itsTotalNumCards = 0;
        } else {
            itsSuits = new String[]{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"};
            itsValues = new String[]{"A", "2"};
            itsTotalNumCards = itsSuits.length * itsValues.length;

            for (String curSuit : itsSuits) {
                for (String curValue : itsValues) {
                    itsCards.push(new Card(curSuit, curValue, true));
                }
            }
        }
        numDecks++;
    }

    public Deck clone() {
        Deck newDeck = new Deck("Empty");
        for (Card card : this.getItsCards()) {
            newDeck.add(card);
        }
        return newDeck;
    }

    public static int getNumDecks() {
        return numDecks;
    }

    public String getItsName() {
        return itsName;
    }

    public String[] getItsSuits() {
        return itsSuits;
    }

    public String[] getItsValues() {
        return itsValues;
    }

    public int getItsTotalNumCards() {
        return itsTotalNumCards;
    }

    public Stack<Card> getItsCards() {
        return itsCards;
    }

    public void setItsName(String name) {
        itsName = name;
    }

    public String toString() {
        String ret = itsName + ": [";
        for (int i = 0; i < itsCards.size(); i++) {
            if (i == 0) {
                ret += itsCards.get(i).toString();
            } else {
                ret += " " + itsCards.get(i).toString();
            }
        }

        ret += "]";

        return ret;
    }

    public Deck cut() {
        return cut(-1);
    }

    public Deck cut(int cardsToCut) {
        if (cardsToCut == -1) {
            cardsToCut = (int) (itsCards.size() * Math.random());
        }
        Deck newDeck = new Deck("Empty");
        for (int i = 0; i < cardsToCut; i++) {
            newDeck.add(this.draw());
        }
        return newDeck;
    }

    public void shuffle() {
        shuffle(100);
    }

    public void shuffle(int passes) {
        shuffle(passes, false);
    }

    public void shuffle(int passes, boolean verbose) {
        for (int j = 0; j < passes; j++) {
            for (int i = 0; i < itsCards.size(); i++) {
                int card1 = i;
                int card2 = (int) (itsCards.size() * Math.random());
                while (card2 == card1) {
                    card2 = (int) (itsCards.size() * Math.random());
                }
                Card temp = (Card) itsCards.get(card1);
                itsCards.set(card1, itsCards.get(card2));
                itsCards.set(card2, temp);
            }
        }
        if (verbose) {
            System.out.println(itsName + " SHUFFLED " + passes + " TIMES.");
        }
    }

    public void organize() {
        organize(false);
    }

    public void organize(boolean verbose) {
        int evaluateThisIndex = 0;
        int nextIndexToBeSortedInto = 0;
        for (int i = 0; i < itsSuits.length; i++) {
            for (int j = 0; j < itsValues.length; j++) {
                while (!(((Card) itsCards.get(evaluateThisIndex)).getItsSuit().equals(itsSuits[i]) && ((Card) itsCards.get(evaluateThisIndex)).getItsValue().equals(itsValues[j]))) {
                    evaluateThisIndex++;
                }

                Card temp = (Card) itsCards.get(nextIndexToBeSortedInto);
                itsCards.set(nextIndexToBeSortedInto, itsCards.get(evaluateThisIndex));
                itsCards.set(evaluateThisIndex, temp);

                evaluateThisIndex = 0;
                nextIndexToBeSortedInto++;
            }
        }

        if (verbose) {
            System.out.println(itsName + " ORGANIZED.");
        }
    }

    public Card draw() {
        Card ret = itsCards.pop();

        return ret;
    }

    public void add(Card par) {
        itsCards.push(par);
    }

    public Hand[] deal(int numHands, int numCardsEach) {
        String[] names = null;
        return deal(numHands, numCardsEach, names);
    }

    public Hand[] deal(int numHands, int numCardsEach, String[] names) {
        return deal(numHands, numCardsEach, names, false);
    }

    public Hand[] deal(int numHands, int numCardsEach, String[] names, boolean verbose) {
        Hand[] ret = new Hand[numHands];

        if (names == null) {
            for (int i = 0; i < numHands; i++) {
                ret[i] = new Hand();
            }
        } else {
            for (int i = 0; i < numHands; i++) {
                ret[i] = new Hand(names[i]);
            }
        }

        for (int i = 0; i < numCardsEach; i++) {
            for (int j = 0; j < numHands; j++) {
                ret[j].addCard(draw());
            }
        }

        if (verbose) {
            System.out.println(numHands + " HANDS OF " + numCardsEach + " CARDS EACH HAVE BEEN DEALT.");
        }

        return ret;
    }
}

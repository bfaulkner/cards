package CardGen;

/**
 * Created by Brandon Faulkner on 8/17/16.
 */
public class PlayingCard{

    private String suit;
    private String value;

    public PlayingCard(){
        this.suit = "";
        this.value = "";
    }

    public PlayingCard(String suit, String value) {
        if (isValidCard(suit, value)) {
            this.suit = suit;
            this.value = value;
        }else {
            this.suit = "joker";
            this.value = "joker";
        }
    }

    public String displayCard() {
        if (value.isEmpty() && suit.isEmpty()) {
            return
                    "┌─────────┐\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "│▒▒▒▒▒▒▒▒▒│\n" +
                            "└─────────┘\n";
        } else if (value.equals("joker")) {
            return
                    "┌─────────┐\n" +
                            "│J        │\n" +
                            "│O        │\n" +
                            "│K   *   J│\n" +
                            "│E  *J*  O│\n" +
                            "│R   *   K│\n" +
                            "│        E│\n" +
                            "│        R│\n" +
                            "└─────────┘\n";
        } else if (value.equals("10")){
            return
                    "┌─────────┐\n" +
                            "│" + value + suit + "      │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + suit + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│      " + suit+ value + "│\n" +
                            "└─────────┘\n";
        }else{
            return
                    "┌─────────┐\n" +
                            "│" + value + suit + "       │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│    " + suit + "    │\n" +
                            "│         │\n" +
                            "│         │\n" +
                            "│       " + suit+ value + "│\n" +
                            "└─────────┘\n";
            }
    }

    public void setPlayingCard(String suit, String value){
        if (isValidCard(suit, value)) {
            this.suit = suit;
            this.value = value;
        }else {
            this.suit = "joker";
            this.value = "joker";
        }
    }

    public String getSuit(){
        return this.suit;
    }

    public String getValue(){
        return this.value;
    }

    public void changeSuit(String suit){
        if (isValidCard(suit, "")) {
            this.suit = suit;
        }else {
            this.suit = "joker";
            this.value = "joker";
        }
    }

    public void changeValue(String value){
        if (isValidCard("", value)) {
            this.value = value;
        }else {
            this.suit = "joker";
            this.value = "joker";
        }
    }

    private boolean isValidCard(String suit, String value){
        switch(suit){
            case "♠":
            case "♣":
            case "♥":
            case "♦":
            case "":
                if (value.equals("")){
                    return false;
                }
                break;
            default:
                return false;
        }
        switch(value){
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "10":
            case "J":
            case "Q":
            case "K":
            case "A":
            case "":
                if (suit.equals("")){
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    public boolean equals(PlayingCard card){
        return (card.getValue().equals(value) && card.getSuit().equals(suit));
    }

}

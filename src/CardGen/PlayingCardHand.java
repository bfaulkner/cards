package CardGen;

/**
 * Created by cn103302 on 8/17/16.
 */
public class PlayingCardHand {
    private PlayingCard[] hand = new PlayingCard[52];
    private int actualLength;

    public PlayingCardHand(PlayingCard[] hand){
        for (int i = 0; i < hand.length; i++){
            this.hand[i] = hand[i];
        }
        actualLength = hand.length;
    }


    public String display(){
        String handStr = "";

        for (int i = 0; i < actualLength; i++){
            handStr += "┌───";
        }
        handStr += "──────┐\n";

        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().equals("10")) {
                handStr += "│" + hand[i].getValue() + hand[i].getSuit();

            } else if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else if (hand[i].getValue().equals("joker")) {
                handStr += "│J  ";
            } else {
                handStr += "│" + hand[i].getValue() + hand[i].getSuit() + " ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        }else{
            handStr += "      │\n";
        }

        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else if (hand[i].getValue().equals("joker")) {
                handStr += "│O  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        }else {
            handStr += "      │\n";
        }
        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else if (hand[i].getValue().equals("joker")) {
                handStr += "│K  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand[actualLength-1].getValue().equals("joker")) {
            handStr += " *   J│\n";
        } else {
            handStr += "      │\n";
        }
        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else if (hand[i].getValue().equals("joker")) {
                handStr += "│E  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand[actualLength-1].getValue().equals("joker")) {
            handStr += "*J*  O│\n";
        } else {
            handStr += " " + hand[actualLength - 1].getSuit() + "    │\n";
        }
        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else if (hand[i].getValue().equals("joker")) {
                handStr += "│R  ";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand[actualLength-1].getValue().equals("joker")) {
            handStr += " *   K│\n";
        } else {
            handStr += "      │\n";
        }

        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand[actualLength-1].getValue().equals("joker")) {
            handStr += "     E│\n";
        } else {
            handStr += "      │\n";
        }

        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            if (hand[i].getValue().isEmpty()) {
                handStr += "│▒▒▒";
            } else {
                handStr += "│   ";
            }
        }
        if (hand[actualLength - 1].getValue().equals("10")){
            handStr += "   " + hand[actualLength - 1].getSuit() + hand[actualLength - 1].getValue() + "│\n";
        }else if (hand[actualLength-1].getValue().isEmpty()) {
            handStr += "▒▒▒▒▒▒│\n";
        } else if (hand[actualLength-1].getValue().equals("joker")) {
            handStr += "     R│\n";
        } else {
            handStr += "    " + hand[actualLength - 1].getSuit() + hand[actualLength - 1].getValue() + "│\n";
        }
        for (int i = 0; i < actualLength; i++) {/////////////////////////////////////////////FOR
            handStr += "└───";
        }
        handStr += "──────┘\n";

        return handStr;
    }

    public int countValues(int aceVal){
        int total = 0;
        for (int i = 0; i < actualLength; i++){
            switch (hand[i].getValue()){
                case "A":
                    if (aceVal == 1){
                        total += 1;
                    }else {
                        total += 11;
                    }
                    break;
                case "K":
                case "Q":
                case "J":
                    total += 10;
                    break;
                case "joker":
                    total = 999;
                case "":
                    break;
                default:
                    total += Integer.parseInt(hand[i].getValue());
                    break;
            }
        }
        return total;
    }

    public void addCard(PlayingCard card){
        hand[actualLength] = card;
        actualLength++;
    }

    public void removeCard(PlayingCard card){
        for (int i = 0; i < actualLength; i++){
            if (hand[i].equals(card)){
                for (int j = i; j < actualLength-1; j++){
                    hand[j] = hand[j+1];
                }
                hand[actualLength-1] = null;
                actualLength--;
            }
        }
    }

}

package CardGen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Brandon Faulkner on 8/17/16.
 */
public class Application {
    public static void main(String args[]) throws IOException {
        PlayingCard KingOfHearts = new PlayingCard("♥", "K");
//        System.out.println(KingOfHearts.displayCard());

        PlayingCard bad = new PlayingCard("a", "3");
//        System.out.println(bad.displayCard());

        PlayingCard BackOfCard = new PlayingCard();

        PlayingCard okCard = new PlayingCard("♥", "2");
        PlayingCard g = new PlayingCard("♥", "A");
        PlayingCard emp = new PlayingCard();
        PlayingCard a = new PlayingCard("♥", "10");
        PlayingCard c = new PlayingCard("♥", "5");
        PlayingCard d = new PlayingCard("♥", "6");
        PlayingCard e = new PlayingCard("♥", "7");
        PlayingCard r = new PlayingCard("♥", "8");
        PlayingCard t = new PlayingCard("♥", "9");
        PlayingCard z = new PlayingCard("♥", "10");
        PlayingCard empt = new PlayingCard();
        PlayingCard a1 = new PlayingCard("♥", "6");
        PlayingCard a2 = new PlayingCard("♥", "7");
        PlayingCard a3 = new PlayingCard("♥", "8");
        PlayingCard a4 = new PlayingCard("♥", "9");
        PlayingCard a5 = new PlayingCard("♥", "10");
        PlayingCard a6 = new PlayingCard();
        PlayingCard bad1 = new PlayingCard("a", "3");
        PlayingCard okCard1 = new PlayingCard("♥", "2");
        PlayingCard g1 = new PlayingCard("♥", "3");
        PlayingCard emp1 = new PlayingCard();
        PlayingCard a12 = new PlayingCard("♥", "10");
        PlayingCard c1 = new PlayingCard("♥", "5");
        PlayingCard d1 = new PlayingCard("♥", "6");
        PlayingCard e1 = new PlayingCard("♥", "7");
        PlayingCard r1 = new PlayingCard("♥", "8");
        PlayingCard t1 = new PlayingCard("♥", "9");
        PlayingCard z1 = new PlayingCard("♥", "10");
        PlayingCard empt1 = new PlayingCard();
        PlayingCard a11 = new PlayingCard("♥", "6");
        PlayingCard a21 = new PlayingCard("♥", "7");
        PlayingCard a31 = new PlayingCard("♥", "8");
        PlayingCard a41 = new PlayingCard("♥", "9");
        PlayingCard a51 = new PlayingCard("♥", "10");
        PlayingCard a61 = new PlayingCard();
        PlayingCard bad11 = new PlayingCard("a", "3");
        PlayingCard bad21 = new PlayingCard("a", "3");

        PlayingCard[] hand = {KingOfHearts, okCard, g, emp, bad, BackOfCard, a, c, d, r, e, t, z, a1, a2, a3, empt, a4, a5, a6, bad1, okCard1,
        g1, emp1, a12, c1, d1, e1, r1, t1, z1, empt1, a11, a21, a31, a41, a51, a61, bad11, bad21}; //40 values
        PlayingCard[] hand2 = {KingOfHearts, okCard, g};
        PlayingCardHand largeHand = new PlayingCardHand(hand);
        PlayingCardHand normalHand = new PlayingCardHand(hand2);
//        System.out.println(normalHand.display());
//        normalHand.addCard(bad21);
//        System.out.println(normalHand.display());
//        normalHand.removeCard(bad21);
//        System.out.println(normalHand.display());
        System.out.println(largeHand.display());
        System.out.println(normalHand.countValues(11));
        Process proc = Runtime.getRuntime().exec("sl");

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()))) {

            String line = "";
            while ((line = reader.readLine()) != null) {
                System.out.print(line + "\n");
            }
        }
        try {
            proc.waitFor();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }
}

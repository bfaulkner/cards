package Dice;

public class Die {
    private static int numDice = 0;
    private String itsName;
    private int itsNumSides;
    private int itsCurrentValue;

    public Die() {
        this(6);
    }

    Die(int numSides) {
        this(numSides, 1);
    }

    Die(int numSides, int currentValue) {
        this(numSides, currentValue, "DIE" + numDice);
    }

    Die(int numSides, int currentValue, String name) {
        itsNumSides = numSides;
        itsCurrentValue = currentValue;
        itsName = name;
        numDice++;
    }

    public int getItsNumSides() {
        return itsNumSides;
    }

    public int getItsCurrentValue() {
        return itsCurrentValue;
    }

    public String getItsName() {
        return itsName;
    }

    public String toString() {
        String ret = "";

        ret += "[" + itsCurrentValue + "]";

        return ret;
    }

    public String toSymbol() {
        String ret = "";

        ret += "┌────────┐\n";
        ret += "│         │\n";
        ret += "│         │\n";
        ret += "│         │\n";
        ret += "└────────┘\n";

        ret += "┌────────┐\n";

        if (itsCurrentValue == 1) {
            ret += "│         │\n";
            ret += "│    O    │\n";
            ret += "│         │\n";
        }

        if (itsCurrentValue == 2) {
            ret += "│ O       │\n";
            ret += "│         │\n";
            ret += "│       O │\n";
        }

        if (itsCurrentValue == 3) {
            ret += "│ O       │\n";
            ret += "│    O    │\n";
            ret += "│       O │\n";
        }

        if (itsCurrentValue == 4) {
            ret += "│ O     O │\n";
            ret += "│         │\n";
            ret += "│ O     O │\n";
        }

        if (itsCurrentValue == 5) {
            ret += "│ O     O │\n";
            ret += "│    O    │\n";
            ret += "│ O     O │\n";
        }

        if (itsCurrentValue == 6) {
            ret += "│ O     O │\n";
            ret += "│ O     O │\n";
            ret += "│ O     O │\n";
        }

        ret += "└────────┘\n";

        return ret;
    }

    public void roll() {
        itsCurrentValue = (int) (itsNumSides * Math.random());
    }
}
